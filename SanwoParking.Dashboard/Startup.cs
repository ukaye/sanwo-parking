﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SanwoParking.Dashboard.Startup))]
namespace SanwoParking.Dashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

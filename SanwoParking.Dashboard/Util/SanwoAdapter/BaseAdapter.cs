﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using SanwoParking.Dashboard.Util.lib;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json.Converters;

namespace SanwoParking.Dashboard.Util.SanwoAdapter
{
    public class BaseAdapter
    {
        protected const string BaseUrl = "http://46.101.56.210/sanwo-service/public/";
        protected string UserId { get; set; }
        protected string AccessToken { get; set; }

        protected HttpClient httpClient;


        public BaseAdapter(string userId = null, string accesstoken = null)
        {
            UserId = userId;
            AccessToken = accesstoken;
            httpClient.BaseAddress = new Uri(BaseUrl);
        }


        public Task<HttpResponseMessage> Request(object parameters, string url, ServiceConstants.HttpMethod httpMethod)
        {
            if (this.AccessToken != null)
            {
                httpClient.DefaultRequestHeaders.Add("i", UserId.ToString());
                httpClient.DefaultRequestHeaders.Add("a", AccessToken);

            }

            url.Trim();

          if (httpMethod == SanwoParking.Dashboard.Util.lib.ServiceConstants.HttpMethod.GET)
            {
                var endPoint = BaseUrl +url + "?";
                endPoint += string.Join("&", parameters);

                var response = httpClient.GetAsync(endPoint);
                

                return response;
            }


          if (httpMethod == ServiceConstants.HttpMethod.POST)
            {
               // var requestMessage = 
            }

            return null;
        }


        //protected HttpRequestMessage GetHttpRequestMessage<T>(T data)
        //{
        //    var mediaType = new MediaTypeHeaderValue("application/json");
        //    var jsonSerializerSettings = new JsonSerializerSettings();
        //    jsonSerializerSettings.Converters.Add(new IsoDateTimeConverter());

        //    var jsonFormatter = new JsonNetFormatter(jsonSerializerSettings);

        //    var requestMessage = new HttpRequestMessage<t>(data, mediaType, new System.Net.Http.Formatting.MediaTypeFormatter[] { jsonFormatter });

        //    return requestMessage;
        //}

    }
}
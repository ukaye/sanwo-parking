﻿using SanwoParking.Dashboard.Models;
using SanwoParking.Dashboard.Util.lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SanwoParking.Dashboard.Util.SanwoAdapter
{
    public class UserAdapter : BaseAdapter
    {
        public Task<HttpResponseMessage> Login(string identifier, string password)
        {
            var loginModel = new LoginModel() {Identifier = identifier, Password = password };
            var request = Request(loginModel, "user/login", ServiceConstants.HttpMethod.POST);

            return request;
           
        }
    }
}